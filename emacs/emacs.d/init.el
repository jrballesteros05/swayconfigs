(require 'package)
;;(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; Comment/uncomment this line to enable MELPA Stable if desired.  See `package-archive-priorities`
;; and `package-pinned-packages`. Most users will not need or want to do this.
(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)
(add-to-list 'custom-theme-load-path (expand-file-name "~/.emacs.d/themes/"))
(load-theme 'adwaita t)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes '(adwaita))
 '(custom-safe-themes
   '("2b8dff32b9018d88e24044eb60d8f3829bd6bbeab754e70799b78593af1c3aba" "b181ea0cc32303da7f9227361bb051bbb6c3105bb4f386ca22a06db319b08882" "3eb93cd9a0da0f3e86b5d932ac0e3b5f0f50de7a0b805d4eb1f67782e9eb67a4" "00445e6f15d31e9afaa23ed0d765850e9cd5e929be5e8e63b114a3346236c44c" "bdb0a37a078bc030ed685e0700b987d296d3473cfe5256c01577a65c67c6c330" "1bdc49116a77e52aaea740cd8e54b93e0bae6c0895dcc36d5c8d1a493e89c78d" "4c56af497ddf0e30f65a7232a8ee21b3d62a8c332c6b268c81e9ea99b11da0d3" default))
 '(package-selected-packages
   '(nim-mode platformio-mode eglot company highlight-indentation airline-themes password-store emojify mastodon rainbow-mode solarized-theme csv-mode yaml-mode powerline anaconda-mode auto-complete magit markdown-mode)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Source Code Pro" :foundry "ADBO" :slant normal :weight normal :height 121 :width normal)))))
;(ac-config-default)
;(add-hook 'python-mode-hook 'anaconda-mode 'java-mode-hook 'eglot-ensure)
(add-hook 'java-mode-hook 'eglot-ensure)
(add-hook 'python-mode-hook 'eglot-ensure)
;(require 'platformio-mode)
(add-hook 'c++-mode-hook 'eglot-ensure)
(add-hook 'c++-mode-hook 'platformio-conditionally-enable)
(require 'powerline)
(powerline-default-theme)
(require 'airline-themes)
;(load-theme 'airline-qwq t)
(load-theme 'airline-cool t)

(ac-config-default)
(define-globalized-minor-mode my-global-rainbow-mode rainbow-mode
  (lambda () (rainbow-mode 1)))

(define-globalized-minor-mode my-global-highlight-indentation-mode highlight-indentation-mode
  (lambda () (highlight-indentation-mode 1)))

(my-global-rainbow-mode 1)
(my-global-highlight-indentation-mode 1)
(global-display-line-numbers-mode)

(defun flyspell-spanish ()
  (interactive)
  (ispell-change-dictionary "es_CO")
  (flyspell-buffer))

(defun flyspell-esperanto ()
  (interactive)
  (ispell-change-dictionary "esperanto")
  (flyspell-buffer))

(defun flyspell-english ()
  (interactive)
  (ispell-change-dictionary "default")
  (flyspell-buffer))

(setq mastodon-instance-url "https://social.linux.pizza"
      mastodon-active-user "jrballesteros05@posteo.es")

;;Identation guides
(highlight-indentation-mode 1)
;;Org
(org-babel-do-load-languages
 'org-babel-load-languages '((python . t)(eshell .t)))
