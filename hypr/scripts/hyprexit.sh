#!/bin/bash

echo "Stopping all user s6 services..."
s6-rc -v 6 -l /run/${USER}/s6-rc -d change default
s6-rc -v 6 -l /run/${USER}/s6-rc -d change wm

echo "Exiting Hyprland"
hyprctl dispatch exit
