(when *initializing*
  (grename "TERM")
  (gnewbg "WWW")
  (gnewbg "KEEPASS")
  (gnewbg "EMACS")
  (gnewbg "OTHER"))
(clear-window-placement-rules)
(setf *dynamic-group-master-split-ratio* 1/2)
