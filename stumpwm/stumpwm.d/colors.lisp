(defvar chucho-base03 "#002b36")
(defvar chucho-base02 "#073642")
(defvar chucho-base01 "#586e75")
(defvar chucho-base00 "#657b83")
(defvar chucho-base0 "#839496")
(defvar chucho-base1 "#93a1a1")
(defvar chucho-base2 "#eee8d5")
(defvar chucho-base3 "#fdf6e3")
(defvar chucho-yellow "#b58900")
(defvar chucho-orange "#cb4b16")
(defvar chucho-red "#dc322f")
(defvar chucho-magenta "#d33682")
(defvar chucho-violet "#6c71c4")
(defvar chucho-blue "#268bd2")
(defvar chucho-cyan "#2aa198")
(defvar chucho-green "#859900")

(setq *colors*
      `(,chucho-base03   ;; 0 black
        ,chucho-red  ;; 1 red
        ,chucho-green  ;; 2 green
        ,chucho-yellow  ;; 3 yellow
        ,chucho-blue  ;; 4 blue
        ,chucho-magenta  ;; 5 magenta
        ,chucho-cyan   ;; 6 cyan
        ,chucho-base3)) ;; 7 white

(when *initializing*
  (update-color-map (current-screen)))
