(in-package :stumpwm)
(setf *mouse-focus-policy* :click)
;(set-font "-xos4-terminus-medium-r-normal-*-20-*-*-*-*-*-*-*")
;; auto screen lock
(run-shell-command "xautolock -time 2 -corners 000- -cornersize 1000 -locker xtrlock")
;; setting wallpaper
(run-shell-command "feh --bg-scale Pictures/Wallpapers/plant-onion.png")
;; load files
(load "~/.stumpwm.d/keybindings.lisp")
(load "~/.stumpwm.d/modeline.lisp")
(load "~/.stumpwm.d/placements.lisp")
(load "~/.stumpwm.d/themes.lisp")
