(load "~/.stumpwm.d/colors.lisp")
(set-border-color        chucho-base03)
(set-focus-color         chucho-base02)
(set-unfocus-color       chucho-base0)
(set-float-focus-color   chucho-base02)
(set-float-unfocus-color chucho-base01)

(set-fg-color chucho-base2)
(set-bg-color chucho-base03)
(set-border-color chucho-blue)

(load "~/quicklisp/setup.lisp")
;(ql:add-to-init-file)
(ql:quickload "clx-truetype")
(load-module "ttf-fonts")
(xft:cache-fonts)
(clx-truetype:get-font-families)
(set-font (make-instance 'xft:font :family "Ubuntu Mono" :subfamily "Regular" :size 13 :antialias t))
