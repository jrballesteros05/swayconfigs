#!/bin/sh
xdg_process=$(ps aux | grep xdg | grep -v grep | wc -l)

if [ $xdg_process -gt 0 ]
then
    ps aux | grep xdg | grep -v grep | awk '{print $2}' | xargs kill -9
fi
