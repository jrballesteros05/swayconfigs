appname=$(swaymsg -t subscribe '["window"]' | jq -r '.container.name')
dateformated=$(TZ=Europe/Madrid date +'%Y-%m-%d %H:%M')
#network=$(ip route get 1.1.1.1 | grep -Po '(?<=dev\s)\w+' | cut -f1 -d ' ')
network=$(ip route get 1.1.1.1 | grep dev | cut -d ' '  -f 5)

ping=$(ping -c 1 www.codeberg.org | tail -1| awk '{print $4}' | cut -d '/' -f 2 | cut -d '.' -f 1)

battery_charge=$(upower --show-info $(upower --enumerate | grep 'BAT') | grep -E "percentage" | awk '{print $2}')
battery_status=$(upower --show-info $(upower --enumerate | grep 'BAT') | grep -E "state" | awk '{print $2}')
light_status=$(light -G)
loadavg_5min=$(cat /proc/loadavg | awk -F ' ' '{print $2}') 
mute_flag=$(pulsemixer --get-mute | awk '{print $1}')
volume=$(pulsemixer --get-volume | awk '{print $1}')

if ! [ $network ]
then
    network_active="⛔"
else
    network_active="⇆"
fi

if [ $battery_status = "discharging" ];
then
    battery_pluggedin='⚠'
else
    battery_pluggedin='⚡'
fi

if [ $mute_flag -eq 1 ];
then
    mute_status="🔇"
else
    mute_status="🔊"
fi

echo "$appname | $mute_status $volume% |  $light_status% | $battery_pluggedin $battery_charge | 🏋 $loadavg_5min | $network_active  $network ($ping ms) | $dateformated"
#echo $dateformated 
